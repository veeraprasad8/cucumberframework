package com.org.api.utility.apistepdefiniation;

import com.org.com.epam.base.ApiEvent;
import com.org.com.epam.base.MyEpamlBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Assert;

public class EmployeesStepFile extends MyEpamlBase {

    public Response empJsonResponse;
    public String empApi = ApiEvent.EMPLOYEE_API;

    @Given("^User provided all the inputs parameters for an api$")
    public void user_provided_all_the_inputs_parameters_for_an_api() throws Exception {
        // Write code here that turns the phrase above into concrete actions
       empJsonResponse =  RestAssured.given().spec(setParams()).when().get(empApi);


    }

    @When("^User hitting an employee get api$")
    public void user_hitting_an_employee_get_api() throws Exception {
        // Write code here that turns the phrase above into concrete actions

        empJsonResponse.prettyPrint();
    }

    @Then("^User should be able to get all the employee information$")
    public void user_should_be_able_to_get_all_the_employee_information() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(empJsonResponse.getStatusCode());
    }

}
